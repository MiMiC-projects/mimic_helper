# MiMiC Helper Scripts
A collection of python scripts to make working with MiMiC easier

Main contibutors:
* Andrea Levy [![Orcid](https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png)](https://orcid.org/0000-0003-1255-859X)

## Packages needed 
All the scripts are coded in python and require standard packages.
 
Anyway, the required packages can be found in the `requirements.txt` and `environment.yml` file. The instructions to install them as a python virtual environment or a conda environment are reported below.

### 1. Python virtual environment
Python has builtin support for virtual environments that can be created and activated as follows
```
python -m venv mimic_helper_venv
. mimic_helper_venv/bin/activate
python -m pip install -r requirements.txt
```
This will create and activate a virtual environment where the required packages have been installed. It will be located in the directory `mimic_helper_venv`, which will be created in the directory from where you run the commands. The virtual environment can be deactivated by running the command
```
deactivate
```
### 2. Conda environment
Conda allows the user to create, export, list, remove, and update environments that have different versions of Python and/or packages installed in them. If you are not familiar with Conda, check [Coonda installation](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html "installation") for details about the installation and [Conda documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html "conda") for more information about conda environments in general.
```
conda env create -f environment.yml
conda activate mimic_helper
```
This will create and activate a conda environment where the required packages have been installed. The conda environment can be deactivated by running the command 
```
conda deactivate
```

## Scripts
All the scripts are contained in the `script` folder and present a help function, providing a brief description and the input needed.
This can be accessed via
```
python3 script_name.py -h
```

The scripts accept positional and optional arguments, to be specified via the command line. 
The scripts in `mimic_helper` are designed to automatize repetitive processes when working with MiMiC.
In particular, they are used for pre- or post-processing of GROMACS and/or CPMD files.

The scripts contained in `mimic_helper` include:
* CPDM scripts
	* `temp_check.py`	- Extract temperature for QM and MM regions along a trajectory
	* `geofile_extract.py`	- Extract the geometry from a trajectory at a selected step
	* `traj_xyz_convert.py`	- Convert a trajectory file into a .xyz file, easily visualizable with e.g. VMD
* MiMiC scripts
	* `benchmarking_order-cutoff.py`	- Automatize the benchmarking of multipole order and cutoff radius for MiMiC runs
